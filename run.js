'use strict';

const express = require('express');
const session = require('express-session');
const expressFlash = require('express-flash');
const db = require('./server/db.js');
const MongoStore = require('connect-mongo')(session);
const bodyParser = require('body-parser');
const nunjucks = require('nunjucks');
const app = express();
const passport = require('passport');

const appController = require('./controllers/app.js');
const auth = require('./controllers/auth.js');
const config = require('./config.js');

const Card = require('./models/card.js').Card;

// Configure nunjucks
nunjucks.configure('views',
  { autoescape: true,
    express: app
  });

// Configure middleware
app.set('trust proxy', 1);
app.use(expressFlash());
app.use(bodyParser.urlencoded({ extended: false }));
app.use(session({
  secret: 'heouoediudorid',
  saveUninitialized: false,
  resave: false,
  store: new MongoStore({url: config.mongoUrl })
}));
app.use(passport.initialize());
app.use(passport.session());

// Custom middleware
app.use(appController.main);

// Serve static resources
app.use('/static', express.static(__dirname + '/static'));

// Routes
app.get('/', appController.index);
app.get('/login', appController.notLoggedIn, auth.getLogin);
app.post('/login', appController.notLoggedIn, auth.postLogin);
app.get('/logout', appController.loggedIn, auth.logout);
app.get('/register', appController.notLoggedIn, auth.getRegister);
app.post('/register', appController.notLoggedIn, auth.postRegister);

let listCards = function(req, res) {
  console.log('listing cards');
  // todo: handle errors
  Card.find({owner: req.user._id})
    .then(function(cards) {
      res.vars.cards = cards;
      res.render('list_cards.html', res.vars);
    })
    .catch(function(err) {
      req.flash('warning', 'failed: ' + err);
      res.redirect('/');
    });
};

app.get('/cards/list', appController.loggedIn, listCards);
app.get('/cards/review', appController.loggedIn, listCards);

app.get('/cards/add', appController.loggedIn, function(req, res) {
  res.render('add_card.html', res.vars);
});

app.post('/cards/add', appController.loggedIn, function(req, res) {
  new Card({owner: req.user._id, front: req.body.front, back: req.body.back}).save()
    .then(function() {
      req.flash('success', 'Card successfully added');
      res.render('add_card.html', res.vars);
    })
    .catch(function(e) {
      req.flash('warning', 'failed: ' + e);
      res.render('add_card.html', res.vars);
    });
});

// Open listening port
let server = app.listen(config.httpPort, function () {
  console.log('Example app listening on port :' + config.httpPort);
})
