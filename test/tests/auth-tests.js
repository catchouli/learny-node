const Browser = require('zombie');
const config = require('../../config.js');

if (config.env == 'testing') {
  console.log('Environment is testing, clearing database');
}

console.log('Running auth tests');

// Connect to localhost
Browser.localhost('learny.click', config.httpPort);

// Data
const username = "TestUser123";
const email = "test@learny.click";
const password = "test1234";

let browser = new Browser();

describe('User visits signup page', function() {
  before(done => browser.visit('/register', done));

  describe('submits form', function() {
    before(function(done) {
      browser
        .fill('username', username)
        .fill('password', password)
        .fill('email', email)
        .pressButton('Register', done);
    });

    it('should be successful', function() { browser.assert.success() });
    it('should see success page', function() { browser.assert.element('.alert-success'); });
  });
});

describe('User visits login page', function() {
  before(done => browser.visit('/login', done));

  describe('logs in with an invalid email', function() {
    before(done => browser.fill('email', email+'1')
                          .fill('password', password)
                          .pressButton('Sign in', done));

    it('should see success page', function() { browser.assert.element('.alert-danger'); });
  });

  describe('logs in with an invalid password', function() {
    before(done => browser.fill('email', email)
                          .fill('password', password+'1')
                          .pressButton('Sign in', done));

    it('should not see success page', function() { browser.assert.element('.alert-danger'); });
  });
  describe('logs in with valid credentials', function() {
    before(done => browser.fill('email', email)
                          .fill('password', password)
                          .pressButton('Sign in', done));

    it('should see success page', function() { browser.assert.element('.alert-success'); });
  });
});
