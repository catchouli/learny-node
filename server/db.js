'use strict';

const config = require('../config.js');
const mongoose = require('mongoose');

mongoose.Promise = require('bluebird');

// Connect to database
let db = mongoose.connection;
db.on('error', console.error);
db.once('open', _ => console.log('connected to db'));
mongoose.connect(config.mongoUrl, { useMongoClient: true });

exports.conn = db;
