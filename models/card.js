'use strict';

const mongoose = require('mongoose');
const config = require('../config.js');

// Create scheme
const CardSchema = new mongoose.Schema({
  owner: {
    type: String,
    required: true,
    trim: true
  },
  front: {
    type: String,
    required: true
  },
  back: {
    type: String,
    required: true
  },
  due: {
    type: Date,
    default: null
  }
});

// Create model
let Card = mongoose.model('Card', CardSchema);
exports.Card = Card;

