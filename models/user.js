'use strict';

const mongoose = require('mongoose');
const bcrypt = require('bcrypt');
const config = require('../config.js');

// Create scheme
const UserSchema = new mongoose.Schema({
  email: {
    type: String,
    required: true,
    unique: true,
    trim: true
  },
  username: {
    type: String,
    required: true,
    unique: true,
    trim: true
  },
  password: {
    type: String,
    required: true
  }
});

// Create model
let User = mongoose.model('User', UserSchema);
exports.User = User;

// Hash a password
User.prototype.hashPassword = function(password) {
  return bcrypt.hashSync(password, config.salt);
}

// Verify a user's password
User.prototype.verifyPassword = function(password) {
  return bcrypt.compareSync(password, this.password);
}
