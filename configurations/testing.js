console.log('Loading testing environment');

const deasync = require('deasync');

// Other settings
exports.httpPort = 15004;
exports.mongoUrl = 'invalid';

// Start in-memory mongodb
console.log('Starting in-memory db');
const MongoInMemory = require('mongo-in-memory');
let port = exports.httpPort+1000;
let mongoServerInstance = new MongoInMemory(port);

mongoServerInstance.start((error, config) => {
  if (error) {
    console.error(error);
  }
  else {
    exports.mongoUrl = mongoServerInstance.getMongouri('learny-testing');
    console.log('In-memory db started at ' + exports.mongoUrl);
  }
});

// End when we receive sigusr2 - allows this to be run constantly with nodemon
process.once('SIGUSR2', function() {
  mongoServerInstance.stop((error) => {
    if (error) {
      console.error(error);
    }
    else {
      console.log('Closing in-memory db');
    }

    process.kill('SIGUSR2');
  });
});

deasync.loopWhile(function() { return exports.mongoUrl == 'invalid'; });

console.log('Config loaded');
