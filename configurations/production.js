console.log('Loading production environment');

exports.httpPort = 14004;
exports.mongoUrl = 'mongodb://localhost:27017/learny-prod';
