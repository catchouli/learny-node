'use strict';

const passport = require('passport');
const LocalStrategy = require('passport-local').Strategy;
const User = require('../models/user.js').User;

// Passport local strategy
passport.use(new LocalStrategy(
  { usernameField: 'email' },
  function(email, password, done) {
    User.find({email: email})
        .then(function(res) {
          if (res.length == 0) {
            return done(null, false);
          }
          else if (res.length == 1) {
            console.log(res[0].verifyPassword(password));
            if (res[0].verifyPassword(password))
              return done(null, res[0]);
            else
              return done(null, false);
          }
          else {
            return done('Multiple records found for email address.');
          }
        });
  }
));

// Serialization
passport.serializeUser(function(user, done) {
  done(null, user);
});

// Deserialization
passport.deserializeUser(function(user, done) {
  done(null, user);
});

// Login form
exports.getLogin = function(req, res) {
  res.vars.page_name = 'login';
  res.render('login.html', res.vars);
};

// Login handler
exports.postLogin = function(req, res) {
  passport.authenticate('local', {
    successRedirect: '/',
    failureRedirect: '/login',
    failureFlash: 'Invalid username or password.',
    successFlash: 'Logged in successfully.'
  })(req, res);
};

// Logout
exports.logout = function(req, res) {
  req.logout();
  res.redirect('/');
};

// Register
exports.getRegister = function(req, res) {
  res.vars.page_name = 'register';
  res.render('register.html', res.vars);
};

// Registration handler
exports.postRegister = function(req, res) {
  // Create new user
  let user = new User({ username: req.body.username,
                         email: req.body.email,
                         password: User.prototype.hashPassword(req.body.password)});

  // Attempt to save new user
  user.save()
    .then(function() {
      req.flash('success', 'Successfully registered. Please log in to continue.');
      res.redirect('/login');
    })
    .catch(function() {
      req.flash('error', 'There was an error with your registration. Please correct the error to continue.');
      res.render('register.html', res.vars);
    });
};

