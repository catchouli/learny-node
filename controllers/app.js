'use strict';

const passport = require('passport');

// Application controller
exports.main = main;
function main(req, res, next) {
  res.vars = {};

  if (typeof req.user !== 'undefined')
    res.vars.user = req.user;

  next();
}

// Index controller
exports.index = function(req, res) {
  res.vars.page_name = 'index';
  res.render('index.html', res.vars);
}

// Checks if a user is logged in before continuing
exports.loggedIn = function(req, res, next) {
  if (req.user) {
    next();
  }
  else {
    // todo: remember requested page
    req.flash('info', 'Please log in to continue.');
    res.redirect('/login');
  }
}

// Checks that a user is not logged in before continuing
exports.notLoggedIn = function(req, res, next) {
  if (!req.user) {
    next();
  }
  else {
    res.redirect('/');
  }
}

